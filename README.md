## Hello there nerdface!

### Meta information
#### Purpose
This document aims to be a shallow but extensive list of resources related to development, maintenance and use of computer related things such as software, peripherals, hardware, etc. Since this document is meant to complement the _tech_n_programming_ channel on the _Social Anxiety (SA) server_ at _Discord_, the scope of topics that are suitable for this list should mirror the scope of the _SA_ channel. 

#### How to contribute
There are two ways you can contribute to this document:
 - (**Easy**) Ask in the channel and someone else can help you add whatever if applicable
 - (**Hard**) Read the contribution guide and get shit done the only way you know
 
The contribution guide shall contain information about the conventions and format of this document, the general idea should be this:

``` 
Give a man a fish and you feed him for a day; teach a man to fish and you feed him for a lifetime
```
Prefer resources that encourage fundamental understanding rather than for example "useful code snippets" or similar.